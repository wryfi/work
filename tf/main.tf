provider "google" {
  project = var.project
  region = "us-central1"
}

terraform {
  backend "gcs" {}
}

resource "google_storage_bucket" "vita-wryfi-storage" {
  name = var.bucket
  location = var.bucket_location
  uniform_bucket_level_access = true
  cors {
    origin          = ["https://*.a.run.app/", "http://localhost:5000", "http://localhost:3000", "https://cv.wryfi.net/", "https://cv-dev.wryfi.net/"]
    method          = ["GET", "HEAD", "OPTIONS"]
    response_header = ["Accept", "Accept-Encoding", "Content-Length", "Content-Type", "Date", "Server", "Transfer-Encoding"]
    max_age_seconds = 60
  }
}

resource "google_storage_bucket_iam_binding" "vita-wryfi-storage-public" {
  bucket = google_storage_bucket.vita-wryfi-storage.name
  role = "roles/storage.objectViewer"
  members = ["allUsers"]
}

resource "google_service_account" "vita-wryfi-service-account" {
  account_id = "vita-service"
  display_name = "vita service"
  description = "vita service account"
}

resource "google_storage_bucket_iam_binding" "vita-wryfi-storage-access" {
  bucket = google_storage_bucket.vita-wryfi-storage.name
  role = "roles/storage.admin"
  members = [format("serviceAccount:%s", google_service_account.vita-wryfi-service-account.email)]
  depends_on = [google_storage_bucket.vita-wryfi-storage, google_service_account.vita-wryfi-service-account]
}

resource "google_cloud_run_service" "vita" {
  name = var.service_name
  location = "us-central1"
  template {
    spec {
      containers {
        image = format("%s:%s", var.image, var.tag)
        resources {
          limits = {
            cpu: "1"
            memory: "512Mi"
          }
        }
        env {
          name = "VITA__GCP__BUCKET"
          value = var.bucket
        }
        env {
          name = "VITA__GCP__PROJECT"
          value = var.project
        }
        env {
          name = "VITA__VITALS__JSON_URL"
          value = var.vita_json_vitals_url
        }
        env {
          name = "VITA__FILE__BASENAME"
          value = var.vita_file_basename
        }
        env {
          name = "VITA__REACT__URL"
          value = format("https://%s", var.domain)
        }
      }
      service_account_name = google_service_account.vita-wryfi-service-account.email
      container_concurrency = 350
    }
    metadata {
      annotations = {
        "autoscaling.knative.dev/maxScale" = "2"
      }
    }
  }
  traffic {
    percent = 100
    latest_revision = true
  }
  autogenerate_revision_name = true
  depends_on = [google_storage_bucket_iam_binding.vita-wryfi-storage-access]
}

resource "google_cloud_run_service_iam_binding" "vita-cloudrun-public" {
  location = "us-central1"
  project = var.project
  service = google_cloud_run_service.vita.name
  role = "roles/run.invoker"
  members = ["allUsers"]
  depends_on = [google_cloud_run_service.vita]
}

resource "google_cloud_run_domain_mapping" "vita-domain" {
  location = "us-central1"
  name = var.domain
  metadata {
    namespace = var.project
  }
  spec {
    route_name = google_cloud_run_service.vita.name
  }
  depends_on = [google_cloud_run_service.vita]
}
