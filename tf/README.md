To apply this configuration:

1. `export TF_ENV=dev`  (or prod, etc.)
2. `terraform init -reconfigure -backend-config=${TF_ENV}.conf`
3. `terraform workspace select vita`
4. `terraform apply -var-file=${TF_ENV}.tfvars -var="tag=0.1.0" `
