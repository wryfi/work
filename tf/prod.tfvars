bucket = "vita-wryfi"
bucket_location = "us"
domain = "cv.wryfi.net"
image = "us-central1-docker.pkg.dev/wryfi-mgmt/docker/vita"
project  = "wryfi-prod"
service_name = "vita"
tag = "latest"
vita_file_basename = "chaumesser"
vita_json_vitals_url = "https://storage.googleapis.com/vita-wryfi/career.json"
