# vita

This project is the code representation of my curriculum vitae (résumé).

It consists of a typescript react application, which loads structured JSON data
from a defined URL and displays it as a dynamic interactive timeline. _See_ 
[career-test.json](career-test.json) for an example of the json schema.

A small python/flask server uses WhiteNoise to serve the React application,
and calls out to pandoc to render the same JSON data into static downloadable
files that can be fed into automated systems or viewed offline.

The React and python applications are deployed together in a [container](Dockerfile)
that currently runs on Google's Cloud Run service, using [Terraform](tf/README.md) to
define and manage the necessary infrastructure.

## frontend

_See_ the frontend [README](frontend/README.md).

## backend

_See_ the backend [README](backend/README.md).

## just

A [justfile](https://just.systems) is provided at the root of the project to help with task execution.
Use `just -l` to view the available recipes. The justfile reads environment variables from a `.env` file
in the project root for convenience.

## license / reuse

You may reuse this code freely under the terms of the MIT license.
