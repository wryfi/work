FROM python:3.12-slim-bookworm as pybuild
RUN apt-get update && apt-get install -y --no-install-recommends unzip git \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*.deb
RUN mkdir -p /tmp/src /tmp/fonts
# COPY the whole source tree because PBR needs access to git.
COPY . /tmp/src
WORKDIR /tmp/src/backend
# Python build needs requirements.txt; generate it out of band *BEFORE* building docker image
# (justfile handles this) so that we don't have to install pipenv here.
RUN pip install build && python3 -m build -w
WORKDIR /tmp/fonts
ADD "https://fonts.google.com/download?family=Lato" /tmp
RUN  unzip /tmp/download  && rm -f /tmp/download && rm -f /tmp/fonts/OFL.txt

FROM node:20-bookworm-slim as npmbuild
ARG hostsDev
ARG hostsProd
ARG jsonUrlDev
ARG jsonUrlLocal
ARG jsonUrlProd
ARG contactUrl
ARG recaptchaKey
RUN mkdir -p /tmp/src
COPY frontend /tmp/src
WORKDIR /tmp/src
RUN yarn install
RUN REACT_APP_HOSTS_DEV=${hostsDev} REACT_APP_HOSTS_PROD=${hostsProd}  \
    REACT_APP_JSON_URL_DEV=${jsonUrlDev} REACT_APP_JSON_URL_PROD=${jsonUrlProd} REACT_APP_JSON_URL_LOCAL=${jsonUrlLocal} \
    REACT_APP_RECAPTCHA_KEY=${recaptchaKey} REACT_APP_CONTACT_URL=${contactUrl} \
	yarn build

FROM python:3.12-slim-bookworm as install
ARG uid=54321
RUN adduser --home /opt/vita --no-create-home --gecos "" --shell /bin/bash --uid ${uid} --disabled-password vita
RUN apt-get update && apt-get install -y --no-install-recommends \
    pandoc texlive texlive-xetex lmodern \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*.deb
COPY --from=pybuild /tmp/src/backend/dist/*.whl /root/
COPY --from=pybuild /tmp/fonts /usr/local/share/fonts
RUN mkdir /opt/vita
COPY --from=npmbuild /tmp/src/build /opt/vita
RUN pip3 install --no-cache-dir /root/*.whl; rm -f /root/*.whl
EXPOSE 8080/tcp
USER ${uid}
CMD ["gunicorn", "-w", "1", "--threads", "8", "--timeout", "0", "-b", ":8080", "--access-logfile", "-", "--error-logfile", "-", "vita:create_app()"]
