# vita frontend

This project was initially bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and yarn.

It uses [chakra-ui](https://chakra-ui.com/) for accessible UI components and [overmind](https://overmindjs.org/) 
for state management and routing.

The frontend reads JSON data from the configured URL in realtime on each request, so CV data can be updated at any time
without rebuilding the application. _See_ [career-test.json](../career-test.json) for the JSON format.

You can set `REACT_APP_JSON_URL_LOCAL` to `/career.json` and put your json file in the [public](public) directory
for local development.

## configuration

A handful of environment variables are read at build time to configure the vita frontend:

```
REACT_APP_CONTACT_URL=https://my-contactagon-instance/messages
REACT_APP_HOSTS_DEV=cv-dev.my-domain.com,vita-asdf12345-uc.a.run.app
REACT_APP_HOSTS_PROD=cv.my-domain.com,vita-54321fdsa-uc.a.run.app
REACT_APP_JSON_URL_DEV='https://storage.googleapis.com/my-dev-bucket/career.json'
REACT_APP_JSON_URL_LOCAL='/career.json'
REACT_APP_JSON_URL_PROD='https://storage.googleapis.com/vita-bucket/career.json'
REACT_APP_RECAPTCHA_KEY='*******'
```

Overmind compares the window location against `REACT_APP_HOSTS_DEV` and `REACT_APP_HOSTS_PROD` to determine whether
the application is running in production, development, or locally, and infers the appropriate JSON URL and
other settings accordingly.

## Available Scripts

In the project directory, you can run:

### `yarn dev`

Runs the app in the development mode and launches the Overmind inspector.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
