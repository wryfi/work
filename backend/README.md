# vita backend

The vita backend is a simple [Flask](https://flask.palletsprojects.com/) application that works with a careers json
file (_see_ [career-test.json](../career-test.json) for the format), pandoc, and a Google Cloud Storage bucket to
render, cache, and serve a CV in various formats (pdf, docx, odt, and plain text). The JSON data is fetched in realtime
from the provided URL, so the contents of the CV may be updated without redeploying the application.

# python setup / install

The vita backend uses [Pipenv](https://pipenv.pypa.io/en/latest/) for depedency management and builds using 
[PBR](https://docs.openstack.org/pbr/).

Set up a development environment by running `pipenv install -d` to create a virtualenv and install both runtime
and development dependencies.

# flask routes

## /

The root URL path serves the static HTML from the configured document root using 
[WhiteNoise](https://whitenoise.readthedocs.io/en/latest/index.html). As deployed in the vita Dockerfile,
the static document root corresponds to the built React frontend app.

## /file

The `/file` route takes a url parameter, `format`, which sould be one of `pdf`, `docx`, `odt`, or `txt`, to look
in the configured GCS bucket to see if a file named `{hash}.{format}` exists, where `hash` is the md5 hash of
the configured JSON data file. If the file already exists, a redirect to the file is returned. If not, a new file
of the requested type is generated from the JSON data file and saved to the bucket, and then a redirect to the
newly-generated file is returned.

## /html and /markdown

The `/html` and `/markdown` routes return an HTML or Markdown representation of the JSON data, respectively. These
routes are for convenience only and are not used directly in the assembled vita application.

# document generation

Pandoc is used to generate the requested document format from an intermediate Markdown representation. The Markdown
is generated from the configured JSON data using [jinja2](https://jinja.palletsprojects.com/) and the
[basic.md.jinja2](templates/basic.md.jinja2) template.

Rather than using pandoc's python bindings, the backend simply shells out to the system pandoc using the python
[sh](https://sh.readthedocs.io/en/latest/) library. Pandoc is included in the vita docker image for this purpose.
Thread-safe locks are used to prevent simultaneous generation of a document in a given format.

# configuration

The vita backend is configured using [cfitall](https://cfitall.readthedocs.io), which allows configuration
through a yml file, json file, or environment variables. Environment variables are used for Cloud Run deployment
in Terraform. The relevant environment variables are as follows:

```
VITA__FILE__BASENAME=cv
VITA__GCP__BUCKET=vita-bucket-name
VITA__GCP__PROJECT=wryfi-bucket-project
VITA__LOG__LEVEL=info
VITA__REACT__PATH=/opt/vita
VITA__REACT__URL=https://cv-dev.local
VITA__VITALS__JSON_URL=https://storage.googleapis.com/vita-bucket-name/career.json
```

# tests

Unit tests using `pytest` are provided in the [tests](tests) directory. 
