from cfitall.registry import ConfigurationRegistry

config = ConfigurationRegistry("vita")

config.set_default("file.basename", "cv")
config.set_default("gcp.bucket", "unknown_bucket")
config.set_default("gcp.project", "unconfigured_project")
config.set_default("log.level", "info")
config.set_default("react.path", "/opt/vita")
config.set_default("react.url", "http://localhost:3000")
config.set_default("vitals.json_url", "http://localhost:3000/career.json")

config.update()
