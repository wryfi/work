import hashlib
import re
import time
from contextlib import contextmanager

import requests
from google.cloud import storage

from vita import logging
from vita.format import generate_file, LOCK_TYPES
from vita.settings import config

logger = logging.getLogger(__name__)


def get_json():
    try:
        response = requests.get(config.get("vitals.json_url"))
        json_hash = hashlib.md5(response.content).hexdigest()
        return response.json(), json_hash
    except requests.HTTPError as ex:
        logger.error(f"error downloading or hashing json: {ex}")


def get_content_type(file_type):
    return {
        "pdf": "application/pdf",
        "docx": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "odt": "application/vnd.oasis.opendocument.text",
        "txt": "text/plain",
    }.get(file_type, "application/data")


@contextmanager
def gcs_bucket():
    client = storage.Client(project=config.get("gcp.project"))
    try:
        yield client.get_bucket(config.get("gcp.bucket"))
    finally:
        client.close()


def blob_exists(blob_name):
    with gcs_bucket() as bucket:
        return blob_name in [blob.name for blob in bucket.list_blobs()]


def get_download(file_type, tries=0, limit=5):
    try:
        with gcs_bucket() as bucket:
            json_data, json_hash = get_json()
            blob_name = f"{json_hash}.{file_type}"
            if blob_exists(blob_name):
                logger.info(f"found file {blob_name} in GCS bucket {bucket.name}")
                return bucket.get_blob(blob_name).public_url
            else:
                logger.info(f"did not find file {blob_name} in GCS bucket {bucket.name}")
                if blob := generate_blob(file_type, blob_name, json_data, tries, limit):
                    return blob.public_url
    except Exception as ex:
        logger.error(f"error getting blob/url: {ex}")


def generate_blob(file_type, blob_name, json_data, tries=0, limit=5):
    lock = LOCK_TYPES[file_type]
    with gcs_bucket() as bucket:
        if blob_exists(blob_name):
            return bucket.get_blob(blob_name)
        if not lock.locked():
            blob_data = generate_file(file_type, json_data)
            logger.info(f"uploading {blob_name} to GCS bucket {bucket.name}")
            return update_blob(file_type, blob_data, blob_name)
        else:
            logger.warning(f"failed to get lock for generating {blob_name}")
            if tries < limit:
                time.sleep(1)
                tries += 1
                return generate_blob(file_type, blob_name, json_data, tries, limit)
            else:
                logger.warning(f"ran out of tries generating blob {blob_name}")


def update_blob(file_type, blob_data, blob_name):
    content_type = get_content_type(file_type)
    try:
        with gcs_bucket() as bucket:
            new_blob = storage.Blob(blob_name, bucket)
            new_blob.upload_from_string(blob_data, content_type=content_type)
            disposition = f'attachment; filename="{config.get("file.basename")}.{file_type}"'
            new_blob.content_disposition = disposition
            new_blob.cache_control = "public, max-age=300"
            new_blob.update()
            clean_up_blobs(blob_name, file_type)
            return new_blob
    except Exception as ex:
        logger.error(f"error uploading blob: {ex}")


def clean_up_blobs(current_blob_name, file_type):
    try:
        with gcs_bucket() as bucket:
            regex = re.compile(r"^[a-f0-9]{32}\." + file_type)
            old_blobs = [
                blob for blob in bucket.list_blobs() if regex.match(blob.name) and blob.name != current_blob_name
            ]
            for blob in old_blobs:
                blob.delete()
    except Exception as ex:
        logger.error(f"error cleaning up blobs: {ex}")
