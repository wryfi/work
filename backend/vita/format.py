import os
import threading

from jinja2 import Template

from vita import logging
from vita.settings import config

logger = logging.getLogger(__name__)

try:
    import sh

    pandoc = sh.pandoc.bake(_return_cmd=True, _tty_out=False, o="-")
except (ImportError, sh.CommandNotFound):
    logger.warning("could not find a local pandoc installation, file conversion will not work")

    def pandoc(*_args, **_kwargs):
        return ""


PATH = os.path.abspath(os.path.dirname(os.path.abspath(__file__)))

LOCK_DOCX = threading.Lock()
LOCK_ODT = threading.Lock()
LOCK_TXT = threading.Lock()
LOCK_PDF = threading.Lock()

LOCK_TYPES = {"docx": LOCK_DOCX, "odt": LOCK_ODT, "txt": LOCK_TXT, "pdf": LOCK_PDF}


def render_html(vitals):
    with open(os.path.join(PATH, "templates/basic.html.jinja2"), "r") as file_:
        template = Template(file_.read())
    return template.render(vitals=vitals, reactUrl=config.get("react.url"))


def render_markdown(vitals):
    with open(os.path.join(PATH, "templates/basic.md.jinja2"), "r") as file_:
        template = Template(file_.read())
    return template.render(vitals=vitals, reactUrl=config.get("react.url"))


def generate_file(file_type, data):
    generator = {"docx": generate_docx, "odt": generate_odt, "txt": generate_txt, "pdf": generate_pdf}.get(
        file_type, generate_pdf
    )
    return generator(render_markdown(data))


def generate_docx(markdown):
    try:
        LOCK_DOCX.acquire()
        ref_doc = os.path.join(PATH, "tex", "custom-reference.docx")
        luafilter = os.path.join(PATH, "tex", "pagebreak.lua")
        docx = pandoc(f="markdown", t="docx", reference_doc=ref_doc, lua_filter=luafilter, _in=markdown)
        return docx.stdout
    finally:
        LOCK_DOCX.release()


def generate_odt(markdown):
    try:
        LOCK_ODT.acquire()
        ref_doc = os.path.join(PATH, "tex", "custom-reference.odt")
        luafilter = os.path.join(PATH, "tex", "pagebreak.lua")
        odt = pandoc(f="markdown", t="odt", reference_doc=ref_doc, lua_filter=luafilter, _in=markdown)
        return odt.stdout
    finally:
        LOCK_ODT.release()


def generate_txt(markdown):
    try:
        LOCK_TXT.acquire()
        luafilter = os.path.join(PATH, "tex", "pagebreak.lua")
        txt = pandoc(f="markdown", t="plain", lua_filter=luafilter, _in=markdown)
        return txt.stdout
    finally:
        LOCK_TXT.release()


def generate_pdf(markdown):
    try:
        LOCK_PDF.acquire()
        extra_tex = os.path.join(PATH, "tex", "customize.tex")
        pdf = pandoc(
            "-f",
            "markdown",
            "-t",
            "pdf",
            "--pdf-engine=xelatex",
            "-V",
            "geometry=margin=0.6in",
            "-V",
            "colorlinks",
            "-V",
            "mainfont=Lato",
            "-H",
            extra_tex,
            _in=markdown,
        )
        return pdf.stdout
    finally:
        LOCK_PDF.release()
