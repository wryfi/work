import json
from pathlib import Path
from unittest.mock import MagicMock, call

import pytest

import vita.storage

TESTS_PATH = Path(__file__).parent.resolve()
BACKEND_PATH = Path(*TESTS_PATH.parts[:-1])
PROJECT_PATH = Path(*BACKEND_PATH.parts[:-1])


class MockBlob:
    def __init__(self, name):
        self.name = name
        self.public_url = f"https://{self.name}"


@pytest.fixture()
def with_json():
    with open(PROJECT_PATH / "career-test.json", "rb") as file_:
        yield json.loads(file_.read())


@pytest.fixture()
def with_json_txt():
    with open(PROJECT_PATH / "career-test.json", "rb") as file_:
        yield file_.read()


def test_get_json(mocker, with_json, with_json_txt):
    mock_get = mocker.patch("vita.storage.requests.get")
    mock_get.return_value.content = with_json_txt
    mock_get.return_value.json.return_value = with_json
    json_data, json_hash = vita.storage.get_json()
    assert json_hash == "f9d4d674c676e36470f9c9f1215d0577"
    assert json_data == with_json


def test_get_content_type():
    assert vita.storage.get_content_type("pdf") == "application/pdf"
    assert (
        vita.storage.get_content_type("docx")
        == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    )
    assert vita.storage.get_content_type("odt") == "application/vnd.oasis.opendocument.text"
    assert vita.storage.get_content_type("txt") == "text/plain"
    assert vita.storage.get_content_type("foobar") == "application/data"


def test_blob_exists(mocker):
    mocker.patch("vita.storage.gcs_bucket").return_value.__enter__.return_value.list_blobs.return_value = [
        MockBlob("f9d4d674c676e36470f9c9f1215d0577.pdf")
    ]
    assert vita.storage.blob_exists("f9d4d674c676e36470f9c9f1215d0577.pdf") is True


def test_blob_exists_not(mocker):
    mocker.patch("vita.storage.gcs_bucket").return_value.__enter__.return_value.list_blobs.return_value = []
    assert vita.storage.blob_exists("foo") is False


def test_generate_blob_that_exists(mocker, with_json):
    mocker.patch("vita.storage.blob_exists").return_value.__enter__.return_value.list_blobs.return_value = True
    mocker.patch("vita.storage.gcs_bucket").return_value.__enter__.return_value.get_blob.return_value = MockBlob(
        "f9d4d674c676e36470f9c9f1215d0577.pdf"
    )
    blob = vita.storage.generate_blob("pdf", "f9d4d674c676e36470f9c9f1215d0577.pdf", with_json)
    assert blob.name == MockBlob("f9d4d674c676e36470f9c9f1215d0577.pdf").name


def test_generate_blob_no_lock(mocker, with_json):
    mocker.patch("vita.storage.gcs_bucket")
    mocker.patch.dict("vita.storage.LOCK_TYPES", {"pdf": MagicMock(locked=MagicMock(return_value=False))})
    mocker.patch("vita.storage.blob_exists", return_value=False)
    mocker.patch("vita.storage.generate_file")
    mocker.patch("vita.storage.update_blob", return_value=MockBlob("f9d4d674c676e36470f9c9f1215d0577.pdf"))
    blob = vita.storage.generate_blob("pdf", "f9d4d674c676e36470f9c9f1215d0577.pdf", with_json)
    assert blob.name == MockBlob("f9d4d674c676e36470f9c9f1215d0577.pdf").name
    vita.storage.generate_file.assert_called_once()


def test_generate_blob_lock_fail_then_success(mocker, with_json):
    mocker.patch("vita.storage.gcs_bucket")
    mocker.patch.dict("vita.storage.LOCK_TYPES", {"pdf": MagicMock(locked=MagicMock(side_effect=[True, False]))})
    mocker.patch("vita.storage.blob_exists", return_value=False)
    mocker.patch("vita.storage.generate_file")
    mocker.patch("vita.storage.update_blob", return_value=MockBlob("f9d4d674c676e36470f9c9f1215d0577.pdf"))
    blob = vita.storage.generate_blob("pdf", "f9d4d674c676e36470f9c9f1215d0577.pdf", with_json)
    assert blob.name == MockBlob("f9d4d674c676e36470f9c9f1215d0577.pdf").name
    vita.storage.generate_file.assert_called_once()


def test_generate_blob_exceeds_retries(mocker, with_json):
    mocker.patch("vita.storage.gcs_bucket")
    mocker.patch.dict("vita.storage.LOCK_TYPES", {"pdf": MagicMock(locked=MagicMock(return_value=True))})
    mocker.patch("vita.storage.blob_exists", return_value=False)
    mocker.patch("vita.storage.logger.warning")
    assert vita.storage.generate_blob("pdf", "f9d4d674c676e36470f9c9f1215d0577.pdf", with_json) is None
    vita.storage.logger.warning.assert_has_calls(
        [
            call("failed to get lock for generating f9d4d674c676e36470f9c9f1215d0577.pdf"),
            call("failed to get lock for generating f9d4d674c676e36470f9c9f1215d0577.pdf"),
            call("failed to get lock for generating f9d4d674c676e36470f9c9f1215d0577.pdf"),
            call("failed to get lock for generating f9d4d674c676e36470f9c9f1215d0577.pdf"),
            call("failed to get lock for generating f9d4d674c676e36470f9c9f1215d0577.pdf"),
            call("failed to get lock for generating f9d4d674c676e36470f9c9f1215d0577.pdf"),
            call("ran out of tries generating blob f9d4d674c676e36470f9c9f1215d0577.pdf"),
        ]
    )


def test_update_blob_successful(mocker):
    mock_bucket = mocker.patch("vita.storage.gcs_bucket")
    mock_blob = mocker.patch("vita.storage.storage.Blob")
    mocker.patch("vita.storage.clean_up_blobs")
    mock_bucket.return_value.__enter__.return_value = MagicMock()
    mock_blob.return_value = mock_blob
    mock_blob.update.return_value = None
    result = vita.storage.update_blob("pdf", "test_data", "f9d4d674c676e36470f9c9f1215d0577.pdf")
    assert result == mock_blob
    mock_blob.upload_from_string.assert_called_once_with("test_data", content_type="application/pdf")
    vita.storage.clean_up_blobs.assert_called_once_with("f9d4d674c676e36470f9c9f1215d0577.pdf", "pdf")


def test_update_blob_error(mocker):
    mock_bucket = mocker.patch("vita.storage.gcs_bucket")
    mock_blob = mocker.patch("vita.storage.storage.Blob")
    mock_logger = mocker.patch("vita.storage.logger.error")
    mocker.patch("vita.storage.clean_up_blobs")
    mock_bucket.return_value.__enter__.return_value = MagicMock()
    mock_blob.return_value = mock_blob
    mock_blob.update.side_effect = Exception("Test")
    assert vita.storage.update_blob("pdf", "test_data", "test_blob") is None
    mock_logger.assert_called_once_with("error uploading blob: Test")


def test_clean_up_blobs(mocker):
    mock_bucket = MagicMock()
    mock_blob = MagicMock()
    mock_blob.name = "current_blob_name"
    mock_bucket.list_blobs.return_value = [mock_blob]

    mocker.patch("vita.storage.re.compile", return_value=MagicMock(match=MagicMock(return_value=True)))
    mocker.patch("vita.storage.gcs_bucket").return_value.__enter__.return_value = mock_bucket
    vita.storage.clean_up_blobs("current_blob_name", "file_type")
    mock_bucket.list_blobs.assert_called_once()
    mock_blob.delete.assert_not_called()

    mock_bucket = MagicMock()
    mock_blob = MagicMock()
    mock_blob.name = "old_blob"
    mock_bucket.list_blobs.return_value = [mock_blob]

    mocker.patch("vita.storage.re.compile", return_value=MagicMock(match=MagicMock(return_value=True)))
    mocker.patch("vita.storage.gcs_bucket").return_value.__enter__.return_value = mock_bucket
    vita.storage.clean_up_blobs("current_blob_name", "file_type")
    mock_bucket.list_blobs.assert_called_once()
    mock_blob.delete.assert_called_once()
