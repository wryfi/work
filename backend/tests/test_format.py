import json
from pathlib import Path

import pytest

import vita.format
from vita import format

TESTS_PATH = Path(__file__).parent.resolve()
BACKEND_PATH = Path(*TESTS_PATH.parts[:-1])
PROJECT_PATH = Path(*BACKEND_PATH.parts[:-1])


class MockPandoc:
    stdout = "hello pandoc"

    def __call__(self, *args, **kwargs):
        return self


@pytest.fixture()
def with_json():
    with open(PROJECT_PATH / "career-test.json", "rb") as file_:
        yield json.loads(file_.read())


def test_render_markdown(with_json):
    markdown = format.render_markdown(with_json)
    assert "# John Doe" in markdown


def test_render_html(with_json):
    html = format.render_html(with_json)
    assert '<h1 id="name" class="name">John Doe, Strong Worker // Happy Person</h1>' in html


def test_generate_file_docx(with_json, mocker):
    mocker.patch("vita.format.generate_docx")
    format.generate_file("docx", with_json)
    vita.format.generate_docx.assert_called_once()


def test_generate_file_odt(with_json, mocker):
    mocker.patch("vita.format.generate_odt")
    format.generate_file("odt", with_json)
    vita.format.generate_odt.assert_called_once()


def test_generate_file_txt(with_json, mocker):
    mocker.patch("vita.format.generate_txt")
    format.generate_file("txt", with_json)
    vita.format.generate_txt.assert_called_once()


def test_generate_file_pdf(with_json, mocker):
    mocker.patch("vita.format.generate_pdf")
    format.generate_file("pdf", with_json)
    vita.format.generate_pdf.assert_called_once()


def test_generate_file_unknown_format(with_json, mocker):
    mocker.patch("vita.format.generate_pdf")
    format.generate_file("pdf", with_json)
    vita.format.generate_pdf.assert_called_once()


def test_generate_docx(with_json, mocker):
    mocker.patch("vita.format.pandoc", side_effect=MockPandoc())
    mocker.patch("vita.format.LOCK_DOCX")
    format.generate_docx(with_json)
    vita.format.pandoc.assert_called_once()
    vita.format.LOCK_DOCX.acquire.assert_called_once()
    vita.format.LOCK_DOCX.release.assert_called_once()


def test_generate_odt(with_json, mocker):
    mocker.patch("vita.format.pandoc", side_effect=MockPandoc())
    mocker.patch("vita.format.LOCK_ODT")
    format.generate_odt(with_json)
    vita.format.pandoc.assert_called_once()
    vita.format.LOCK_ODT.acquire.assert_called_once()
    vita.format.LOCK_ODT.release.assert_called_once()


def test_generate_txt(with_json, mocker):
    mocker.patch("vita.format.pandoc", side_effect=MockPandoc())
    mocker.patch("vita.format.LOCK_TXT")
    format.generate_txt(with_json)
    vita.format.pandoc.assert_called_once()
    vita.format.LOCK_TXT.acquire.assert_called_once()
    vita.format.LOCK_TXT.release.assert_called_once()


def test_generate_pdf(with_json, mocker):
    mocker.patch("vita.format.pandoc", side_effect=MockPandoc())
    mocker.patch("vita.format.LOCK_PDF")
    format.generate_pdf(with_json)
    vita.format.pandoc.assert_called_once()
    vita.format.LOCK_PDF.acquire.assert_called_once()
    vita.format.LOCK_PDF.release.assert_called_once()
